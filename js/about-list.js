/**
 * AboutListItem - list item with facts about player or team
 *
 * @constructor
 * @param {string} title - title of line items
 */
var AboutListItem = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function() {
        return (
          <li>
              <div className="title">{this.props.title}</div>
              {this.props.children}
          </li>
        );
    }
});


/**
 * AboutListModule - list of facts about a player or a team
 *
 * @constructor
 * @param {string} title - the lists headline
 */
var AboutListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function () {
        return (
            <section className="about-list-module">
                <h3><span className="col-title">{this.props.title}</span></h3>
                <ul>
                    {this.props.children}
                </ul>
            </section>
        );
    }
});
