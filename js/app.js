var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var benchData = {
    home: [
        {name: "Name Namesen", number: 1, jersey: "img/test-jersey.png", position: "Mål"},
        {name: "Name Namesen", number: 2, jersey: "img/test-jersey.png", position: "Mål"},
        {
            name: "Name Namesen",
            number: 3,
            jersey: "img/test-jersey.png",
            position: "Mål",
            incidents: IncidentEnum.Goal
        },
        {name: "Name Namesen", number: 4, jersey: "img/test-jersey.png", position: "Mål"},
        {
            name: "Name Namesen",
            number: 5,
            jersey: "img/test-jersey.png",
            position: "Mål",
            incidents: IncidentEnum.Yellow | IncidentEnum.Substitute
        },
        {name: "Name Namesen", number: 6, jersey: "img/test-jersey.png", position: "Mål"}
    ],
    away: [
        {name: "Name Namesen", number: 1, jersey: "img/test-jersey.png", position: "Mål"},
        {name: "Name Namesen", number: 2, jersey: "img/test-jersey.png", position: "Mål"},
        {name: "Name Namesen", number: 3, jersey: "img/test-jersey.png", position: "Mål"}
    ]
};

var trainerData = {
    home: [
        {name: "Name Namesen", jersey: "img/test-jersey.png", position: "Træner"}
    ],
    away: [
        {name: "Name Namesen", jersey: "img/test-jersey.png", position: "Træner"}
    ]
};

var ReactView = React.createClass({
    render: function () {
        return (
            <div className="react-view">
                <h2>Standings list module</h2>
                <StandingsListModule />


                <h2>Previous matches list module</h2>
                <PreviousMatchesListModule title="Tidligere kampe">
                    <PreviousMatchesListItem team="Chelsea">
                        <PreviousMatchBubble state="lost" location="away"/>
                        <PreviousMatchBubble state="tie" location="away"/>
                        <PreviousMatchBubble state="won" location="away"/>
                        <PreviousMatchBubble state="lost" location="home"/>
                        <PreviousMatchBubble state="tie" location="home"/>
                        <PreviousMatchBubble state="won" location="home"/>
                        <PreviousMatchBubble state="won" location="home" isLastMatch={true}/>
                    </PreviousMatchesListItem>
                    <PreviousMatchesListItem team="Manchester United">
                        <PreviousMatchBubble state="lost" location="away"/>
                        <PreviousMatchBubble state="tie" location="away"/>
                        <PreviousMatchBubble state="won" location="away"/>
                        <PreviousMatchBubble state="lost" location="home"/>
                        <PreviousMatchBubble state="tie" location="home"/>
                        <PreviousMatchBubble state="won" location="home"/>
                        <PreviousMatchBubble state="won" location="home" isLastMatch={true}/>
                    </PreviousMatchesListItem>
                </PreviousMatchesListModule>

                <h2>Lineup module</h2>
                <LineUpModule homeLineUp="4-2-3-1" awayLineUp="4-2-3-1" homeName="Manchester United"
                              awayName="Chelsea"/>

                <h2>Bench list module</h2>
                <BenchListModule title="På bænken" stadium="Stadio Georgios" homeTeam="Olympiakos Piraeus"
                                 awayTeam="Bayern München" playerData={benchData}/>
                <BenchListModule title="Træner/Manager" playerData={trainerData}/>

                <h2>About list module</h2>
                <AboutListModule title="Om">
                    <AboutListItem title="Født">6. august 1983 (31 år), Rotterdam, Holland</AboutListItem>
                    <AboutListItem title="Højde">183 cm</AboutListItem>
                    <AboutListItem title="Position">Angriber</AboutListItem>
                </AboutListModule>

                <h2>Statistic list module</h2>
                <StatisticListModule title="Generelt spil">
                    <StatisticListItem caption="Besiddelse" valueLabel="45,9%" percent={45.9}/>
                    <StatisticListItem caption="Dueller vundet" valueLabel="51,9%" percent={51.9}/>
                    <StatisticListItem caption="Luft dueller vundet" valueLabel="25,0%" percent={25}/>
                    <StatisticListItem caption="Offsides" valueLabel="90,0%" percent={90}/>
                    <StatisticListItem caption="Hjørnespark" valueLabel="65,0%" percent={65}/>
                    <StatisticListItem caption="Clean sheets" valueLabel="38,4%" percent={38.4}/>
                </StatisticListModule>

                <h2>Match list module</h2>
                <MatchListModule title="Superligaen">
                    <MatchListItem homeName="Lyngby BK" awayName="FC København">
                        <Scoreboard status="before" isSmall={true}/>
                    </MatchListItem>
                    <MatchListItem homeName="Lyngby BK" awayName="FC København">
                        <Scoreboard status="before" isSmall={true}/>
                    </MatchListItem>
                    <MatchListItem homeName="Lyngby BK" awayName="FC København">
                        <Scoreboard status="before" isSmall={true}/>
                    </MatchListItem>
                    <MatchListItem homeName="Lyngby BK" awayName="FC København">
                        <Scoreboard status="before" isSmall={true}/>
                    </MatchListItem>
                </MatchListModule>

                <h2>Scoreboard</h2>
                <Scoreboard status="before"/>
                <Scoreboard status="during" homeScore={2} awayScore={1} time={77} half={1}/>
                <Scoreboard status="after" homeScore={4} awayScore={3} homeRedCards={2}/>
                <Scoreboard status="before" isSmall={true}/>
                <Scoreboard status="during" homeScore={2} awayScore={1} time={45} breakTime={10} half={0}
                            isBreak={true} isSmall={true} awayRedCards={1}/>
                <Scoreboard status="after" homeScore={4} awayScore={3} isSmall={true}/>
                <Scoreboard status="after" homeScore={0} awayScore={0} isSmall={true}/>

                <h2>TV list module</h2>
                <TVListModule title="I dag, mandag" date="11. november" />
                {/*
                <h2>Fixed header test</h2>
                <FixedHeader title="Top container"/>
                <FixedHeader title="Middle container" />
                <FixedHeader title="Bottom container" />
                */}
                <h2>Debat</h2>
                <Debat />

                <h2>Match Highlights</h2>
                <MatchHighlightsModule />

                <h2>Compare list</h2>
                <CompareListModule title="Generelt spil" />
            </div>
        );
    }
});

ReactDOM.render(
    <ReactView />,
    document.getElementById("react-container")
);
