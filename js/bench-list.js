/**
 * Players name and position.
 *
 * Helper for BenchListModule
 *
 * @constructor
 * @param {string} name - name of the player
 * @param {string} location - home or away
 * @param {number} position - display name of players position fx. "Mål", "Forsvar", "Træner"
 * @param {IncidentEnum=} incidents - incident flag
 */

var BenchListNameColumn = React.createClass({
    render: function () {
        var clsName = "player-name-" + this.props.location;
        return (
            <div className={clsName}>
                {this.props.name}
                <small>{this.props.position}</small>
                <PlayerIncidents incidents={this.props.incidents} />
            </div>
        );
    }
});


/**
 * Head 2 head list of players for two teams.
 *
 * @constructor
 * @param {string} title - the lists headline
 * @param {string=} stadium - name of the stadium
 * @param {string=} homeTeam - name of the home team
 * @param {string=} awayTeam - name of the away team
 * @param {object} playerData - player data in format:
 * {
 *   home: [{ name: "string", number: 1, jersey: "url", position: "pos", incidents=IncidentEnum }, ...}],
 *   away: [{ name: "string", number: 1, jersey: "url", position: "pos", incidents=IncidentEnum }, ...}]
 * }
 */
var BenchListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired,
        stadium: React.PropTypes.string,
        homeTeam: React.PropTypes.string,
        awayTeam: React.PropTypes.string,
        playerData: React.PropTypes.object.isRequired,
        initIncidents: React.PropTypes.arrayOf(React.PropTypes.string)
    },
    render: function () {
        var homeArr = this.props.playerData.home,
            awayArr = this.props.playerData.away,
            len = Math.max(homeArr.length, awayArr.length),
            listArr = [],
            teams = [],
            hItem, aItem,
            homeJersey, awayJersey,
            homeName, awayName,
            homeClsName, awayClsName;

        // create list items
        for (var i = 0; i < len; i++) {
            hItem = homeArr[i];
            aItem = awayArr[i];

            homeJersey = hItem ? (<PlayerJersey jerseyUrl={hItem.jersey} number={hItem.number} location="home"/>) : [];
            awayJersey = aItem ? (<PlayerJersey jerseyUrl={aItem.jersey} number={aItem.number} location="away"/>) : [];
            homeName = hItem ? (<BenchListNameColumn name={hItem.name} position={hItem.position} location="home" incidents={hItem.incidents} />) : [];
            awayName = aItem ? (<BenchListNameColumn name={aItem.name} position={aItem.position} location="away" incidents={aItem.incidents} />) : [];

            homeClsName = "col-2" + (Array.isArray(homeName) ? " is-empty" : "");
            awayClsName = "col-3" + (Array.isArray(awayName) ? " is-empty" : "");

            listArr.push(
                <li key={i}>
                    <div className="col-1">{homeJersey}</div>
                    <div className={homeClsName}>{homeName}</div>
                    <div className={awayClsName}>{awayName}</div>
                    <div className="col-4">{awayJersey}</div>
                </li>
            );
        }

        // if there is a home team or away team name defined, create a teams div
        if( this.props.homeTeam && this.props.awayTeam && this.props.homeTeam.length > 0 && this.props.awayTeam.length > 0) {
            teams = (
                <div className="teams">
                    <span>{this.props.homeTeam}</span>
                    <span>{this.props.awayTeam}</span>
                </div>
            )
        }

        return (
            <section className="bench-list-module">
                <h3>
                    <span className="col-title">{this.props.title}</span>
                    <span className="col-stadium">{this.props.stadium}</span>
                </h3>
                {teams}
                <ul>
                    {listArr}
                </ul>
            </section>
        );
    }
});
