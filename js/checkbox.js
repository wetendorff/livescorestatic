var Checkbox = React.createClass({
    getInitialState: function() {
        return {
            checked: true,
            isLoading: false
        };
    },

    handleClick: function() {
        this.setState({checked: !this.state.checked });
    },

    render: function () {
        return (
            <div className={"checkbox-pattern " + (this.state.checked ? "is-checked" : "")} onClick={this.handleClick}>
                <div className="checkbox-knob"></div>
            </div>
        )
    }
});