var CompareListItem = React.createClass({
    propTypes: {
        caption: React.PropTypes.string.isRequired,
        labelLeft: React.PropTypes.string.isRequired,
        labelRight: React.PropTypes.string.isRequired,
        prcLeft: React.PropTypes.number.isRequired,
        prcRight: React.PropTypes.number.isRequired      // value between 0 and 100
    },

   render: function() {
       return (
           <li>
               <div className="headline">
                   <span className="left-number">{this.props.labelLeft}</span>
                   <span className="caption">{this.props.caption}</span>
                   <span className="right-number">{this.props.labelRight}</span>
               </div>
               <div className="distribution">
                   <div className="left-bar">
                       <span style={{width: this.props.prcLeft + '%'}}></span>
                   </div>
                   <div className="right-bar is-mute">
                       <span style={{width: this.props.prcRight + '%'}}></span>
                   </div>
               </div>
           </li>
       );
   }
});

var CompareListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function () {
        return (
            <section className="compare-list-module">
                <h3><span className="col-title">{this.props.title}</span></h3>
                <ul>
                    <CompareListItem prcLeft={54.1} labelLeft="54,1%" caption="Besiddelse" prcRight={45.9} labelRight="45,9%" />
                    <CompareListItem prcLeft={33.3} labelLeft="33,3%" caption="Oste" prcRight={66.7} labelRight="66,7%" />
                </ul>
            </section>
        );
    }
});
