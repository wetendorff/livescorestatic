var Debat = React.createClass({

    render: function () {
        return (
            <div className="debat-module">
                <ul>
                    <li>
                        <div className="col1">
                            <span className="timestamp">18:46</span>
                            <div className="author">Gustav</div>
                        </div>
                        <div className="col2">
                            Brugerkommentar fra Gustav:
                            <p>Det var første hjemmebanenederlag ja, Jonas, men ikke første nederlag. Det sørgede Wolfsburg for i 4-1 kampen. God dækning og god søndag.</p>
                        </div>
                    </li>
                    <li className="is-reply">
                        <div className="col1"></div>
                        <div className="col2">
                            Svar fra Jonas Sørvin Kristiansen:
                            <p>Du har ret. Mente dog, at det var Bayerns første nedelag på hjemmebanen. Det fik jeg dog ikke skrevet i min seneste opdatering - beklager :)</p>
                        </div>
                    </li>

                    <li className="is-twitter">
                        <div className="col1">
                            <span className="timestamp">18:46</span>
                            <div className="author">Gustav</div>
                        </div>
                        <div className="col2">
                            <i className="fa fa-twitter twitter-icon"></i>
                            <b>NYT <a href="#">@NYT</a></b>
                            <p>The Learning Network: Should High Schools Drop Football Because Too Many Players Are Getting Injured?. <a href="#">rss.nytimes.com/c/34625/f/6403…</a></p>
                        </div>
                    </li>

                    <li className="is-twitter-reply">
                        <div className="col1"></div>
                        <div className="col2">
                            Svar fra Jonas Sørvin Kristiansen:
                            <p>Du har ret. Mente dog, at det var Bayerns første nedelag på hjemmebanen. Det fik jeg dog ikke skrevet i min seneste opdatering - beklager :)</p>
                        </div>
                    </li>

                    <li className="is-twitter">
                        <div className="col1">
                            <span className="timestamp">18:46</span>
                            <div className="author">ian bremmer</div>
                        </div>
                        <div className="col2">
                            <i className="fa fa-twitter twitter-icon"></i>
                            <b>ian bremmer  <a href="#">@ian bremmer</a></b>
                            <p>TThe NYT does Trump. Appropriately. <a href="#">pic.twitter.com/V98lfUjx7G</a></p>
                            <img src="https://pbs.twimg.com/media/CQGPdt0UYAAXXt5.jpg" className="content-image" />
                        </div>
                    </li>

                    <li className="is-twitter-reply">
                        <div className="col1"></div>
                        <div className="col2">
                            Svar fra Jonas Sørvin Kristiansen:
                            <p>Du har ret. Mente dog, at det var Bayerns første nedelag på hjemmebanen. Det fik jeg dog ikke skrevet i min seneste opdatering - beklager :)</p>
                        </div>
                    </li>

                    <li className="is-instagram">
                        <div className="col1">
                            <span className="timestamp">20:03</span>
                            <div className="author">Jakob S</div>
                        </div>
                        <div className="col2">
                            <i className="fa fa-instagram instagram-icon"></i>
                            <p>#donaldtrump #horse #lookalike #trump #trump2016 #crazyhair #follow #followme #followthisaccount #instafunny #donaldtrumplookalike #donald #donald2016 #donaldtrump2016</p>
                            <img src="https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e35/11910534_1498933260419569_1147470771_n.jpg" className="content-image" />
                        </div>
                    </li>

                    <li className="is-instagram-reply">
                        <div className="col1"></div>
                        <div className="col2">
                            Svar fra Jonas Sørvin Kristiansen:
                            <p>Du har ret. Mente dog, at det var Bayerns første nedelag på hjemmebanen. Det fik jeg dog ikke skrevet i min seneste opdatering - beklager :)</p>
                        </div>
                    </li>

                    <li className="is-youtube">
                        <div className="col1">
                            <span className="timestamp">21:32</span>
                            <div className="author">Lars N</div>
                        </div>
                        <div className="col2">
                            <i className="fa fa-youtube youtube-icon"></i>
                            <iframe style={{width: "290px", height: "217px"}} src="https://www.youtube.com/embed/uvfvXo5iLok?wmode=transparent" frameBorder="0" allowFullScreen=""></iframe>
                        </div>
                    </li>

                    <li>
                        <div className="col1">
                            <span className="timestamp">21:54</span>
                            <div className="author">Lars N</div>
                        </div>
                        <div className="col2">
                            <p>Starten i Shanghai for 33 runder siden. (Foto: AP)</p>
                            <img src="http://cdnmo.coveritlive.com/media/image/201309/phpnzlrx3image.jpeg" className="content-image" />
                        </div>
                    </li>

                    <li className="is-score-radars">
                        <div className="col1">
                            <span className="timestamp">34'</span>
                            <div>
                                <span className="event-icon-pattern is-goal"></span>
                            </div>
                        </div>
                        <div className="col2">
                            Elmander lægger bolden modsat og så er Brøndby på 4-0. Det lugter af lussing.
                        </div>
                    </li>

                    <li className="is-score-radars">
                        <div className="col1">
                            <span className="timestamp">34'</span>
                            <div>
                                <span className="event-icon-pattern is-yellow-card"></span>
                            </div>
                        </div>
                        <div className="col2">
                            Nordmanden får gult kort for at smide med en stol ude på trænerbænken. Hidsigt.
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
});
