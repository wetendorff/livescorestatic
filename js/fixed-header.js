var FixedHeader = React.createClass({
    mixins: [FixedHeaderMixin],

    componentDidMount: function () {
        this.addFixedHeader(this.refs.container, this.refs.header, 0);
        this.addFixedHeader(this.refs.container2, this.refs.header2, 0);
    },

    render: function () {
        return (
            <div>
                <div className="fixed-header-pattern" ref="container">
                    <h3 ref="header">{this.props.title}</h3>
                    <ul>
                        <li>Line 1</li>
                        <li>Line 2</li>
                        <li>Line 3</li>
                        <li>Line 4</li>
                        <li>Line 5</li>
                        <li>Line 6</li>
                        <li>Line 7</li>
                        <li>Line 8</li>
                        <li>Line 9</li>
                        <li>Line 10</li>
                    </ul>
                </div>
                <div className="fixed-header-pattern" ref="container2">
                    <h3 ref="header2">Sub {this.props.title}</h3>
                    <ul>
                        <li>Line 1</li>
                        <li>Line 2</li>
                        <li>Line 3</li>
                        <li>Line 4</li>
                        <li>Line 5</li>
                        <li>Line 6</li>
                        <li>Line 7</li>
                        <li>Line 8</li>
                        <li>Line 9</li>
                        <li>Line 10</li>
                    </ul>
                </div>
            </div>
        );
    }
});
