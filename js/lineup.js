/**
 * Player name, jersey with player number and list of plays specific attributes.
 *
 * @constructor
 * @param {int} id - unique id for player
 * @param {string} location - home or away
 * @param {int} number - players jersey number
 * @param {string} name - players name
 * @param {IncidentEnum=} incidents - incident flag
 */
var PlayerModule = React.createClass({
    propTypes: {
        id: React.PropTypes.number.isRequired,
        location: React.PropTypes.oneOf(['home', 'away']).isRequired,
        number: React.PropTypes.number.isRequired,
        name: React.PropTypes.string.isRequired,
        incidents: React.PropTypes.number
    },

    render: function () {
        var clsName = "player player-" + this.props.id;

        return (
            <div className={clsName}>
                <PlayerJersey jerseyUrl="img/test-jersey.png" number={this.props.number}
                              location={this.props.location}/>
                <PlayerIncidents incidents={this.props.incidents}/>
                <div className="player-name">{this.props.name}</div>
            </div>
        );
    }
});


/**
 * LineUpModule - Visual presentation of the two teams line up.
 *
 * @constructor
 * @param {string} homeName - home teams name
 * @param {string} awayName - away teams name
 * @param {string} homeLineUp - home teams line up fx. 4-4-2
 * @param {string} awayLineUp - away teams line up fx. 4-4-2
 */
var LineUpModule = React.createClass({
    propTypes: {
        homeName: React.PropTypes.string.isRequired,
        awayName: React.PropTypes.string.isRequired,
        homeLineUp: React.PropTypes.string.isRequired,
        awayLineUp: React.PropTypes.string.isRequired
    },

    getInitialState: function () {
        return {
            width: 375, 	               // field width in pixels
            height: 800,  	               // field height in pixels
            contentWidth: 375 - 20,        // content width in pixels
            contentHeight: 800 / 2 - 20,   // content height in pixels
            contentXOffset: 10, 		   // content x offset in pixels
            contentYOffset: 10,            // content y offset in pixels
            animDelay: 50, 	               // delay between each player animation
            homeLineUp: this.props.homeLineUp,
            awayLineUp: this.props.awayLineUp
        };
    },

    render: function () {
        var players = [];
        for (var i = 1; i <= 22; i++) {
            players.push(
                <PlayerModule name="Lastname" number={(i-1)%11+1} location={i<=11 ? "home" : "away"}
                              incidents={IncidentEnum.Red} id={i} key={i}/>
            )
        }
        return (
            <section className="lineup-module">
                <h3><span className="col-title">Start opstilling</span></h3>

                <div className="home-team">
                    <span className="name">{this.props.homeName}</span>
                    <span className="line-up">{this.state.homeLineUp}</span>
                </div>
                <div className="line-up-container">
                    {players}
                </div>
                <div className="away-team">
                    <span className="name">{this.props.awayName}</span>
                    <span className="line-up">{this.state.awayLineUp}</span>
                </div>
            </section>
        );
    },

    componentDidMount: function () {
        this.update();
    },

    componentDidUpdate: function () {
        this.update();
    },

    update: function() {
        var homeLineUp = ("1-" + this.state.homeLineUp).split('-'),
            bottomLineUp = ("1-" + this.state.awayLineUp).split('-');
        this.layoutLineUp(homeLineUp, false);
        this.layoutLineUp(bottomLineUp, true);
    },


    layoutLineUp: function (lineUp, isAwayTeam) {
        var rows = lineUp.length,
            pIndex = 1,
            i, j;

        for (i = 0; i < rows; i++) {
            var cols = lineUp[i];
            for (j = 0; j < cols; j++) {
                this.setPlayerPosition(pIndex, i, j, rows, cols, isAwayTeam);
                pIndex++;
            }
        }
    },

    setPlayerPosition: function (idx, row, col, rows, cols, isAwayTeam) {
        if (isAwayTeam) idx += 11;

        var p = ReactDOM.findDOMNode(this).querySelector('.player-' + idx),
            pw = p.clientWidth,
            ph = p.clientHeight,
            rh = this.state.contentHeight / rows,
            dh = rh - ph,
            doff = dh / (rows - 1),
            yp, xw, xp;

        yp = this.state.contentYOffset + (rh * row) + (doff * row);

        xw = this.state.contentWidth / cols;
        xp = this.state.contentXOffset + xw * col + (xw - pw) * .5;

        // if away team inverse the positions
        if (isAwayTeam) {
            yp = this.state.height - yp - ph;
            xp = this.state.width - pw - xp;
        }

        p.style[prefix.js + "Transform"] = "translate3d(" + xp + "px, " + yp + "px, 0)";
        p.style[prefix.js + "TransitionDelay"] = (idx * 50) + "ms";
    }
});
