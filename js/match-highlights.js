var MatchHighlightsModule = React.createClass({

    render: function () {
        return (
            <section className="match-highlights-module">
                <ul>
                    <li className="event is-end">
                        <div className="event-description"></div>
                        <div className="event-pointer"><i className="icon-whistle icon-whistle-sm"></i></div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-ekstra-time">
                        <div className="event-description"></div>
                        <div className="event-pointer">+3</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-substitution is-away-team">
                        <div className="event-description">
                            Name Namesen
                            <small>Name Namesen</small>
                        </div>
                        <div className="event-pointer has-right-arrow">55´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-substitution is-home-team">
                        <div className="event-description">
                            Name Namesen
                            <small>Name Namesen</small>
                        </div>
                        <div className="event-pointer has-left-arrow">55´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-substitution is-home-team">
                        <div className="event-description">
                            Name Namesen
                            <small>Name Namesen</small>
                        </div>
                        <div className="event-pointer has-left-arrow">55´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-yellow-red-card is-away-team">
                        <div className="event-description">
                            Name Name
                            <small>Andet gule kort</small>
                        </div>
                        <div className="event-pointer has-right-arrow">63´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-goal is-home-team">
                        <div className="event-description">
                            2-0 Manchester U
                            <small>Straffe</small>
                        </div>
                        <div className="event-pointer has-left-arrow">65´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-yellow-card is-away-team">
                        <div className="event-description">
                            Name Namesen
                            <small>Gult kort</small>
                        </div>
                        <div className="event-pointer has-right-arrow">25´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-red-card is-home-team">
                        <div className="event-description">
                            Name Namesen
                            <small>Rødt kort</small>
                        </div>
                        <div className="event-pointer has-left-arrow">22´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-break">
                        Første halvleg 1-0
                    </li>
                    <li className="event is-goal is-home-team">
                        <div className="event-description">
                            1-0 Manchester U
                        </div>
                        <div className="event-pointer has-left-arrow">33´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-penalty is-home-team">
                        <div className="event-description">
                            <small>Straffespark tildelt</small>
                        </div>
                        <div className="event-pointer has-left-arrow">33´</div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-start">
                        <div className="event-description"></div>
                        <div className="event-pointer"><i className="icon-whistle icon-whistle-sm"></i></div>
                        <div className="event-icon"></div>
                    </li>
                    <li className="event is-break">
                        Optakt
                    </li>
                </ul>
                <footer>
                    Kampen startede kl. 20.00
                    <small>10/06/2015 (Onsdag)</small>
                </footer>
            </section>

        );
    }
});

