/**
 * Banner shown as slide in MatchListModule
 *
 * @constructor
 * @param {function} nextSlide - debug callback only for test purpose
 */
var MatchListItemBanner = React.createClass({
    render: function () {
        return (
            <img className="match-list-banner-item" src="img/banner.png" style={{display: "block"}}
                 onClick={this.props.nextSlide}/>
        )
    }
});

/**
 * The main list item in MatchListModule. Shows match status, goals, progress, red cards, etc.
 *
 * @constructor
 * @param {function} nextSlide - debug callback only for test purpose
 * @param {string} homeName - name of the home team
 * @param {string} awayName - name of the visiting team
 * @param {string=} homeLogo - url to home team logo
 * @param {string=} awayLogo - url to visiting team logo
 * @param children - one Scoreboard
 */
var MatchListItemScoreboard = React.createClass({
    propTypes: {
        homeName: React.PropTypes.string.isRequired,
        awayName: React.PropTypes.string.isRequired,
        homeLogo: React.PropTypes.string,
        awayLogo: React.PropTypes.string
    },
    getDefaultProps: function () {
        return {
            homeLogo: "img/fallback-logo-list.png",
            awayLogo: "img/fallback-logo-list.png"
        }
    },
    render: function () {
        return (
            <div className="match-line-scoreboard-item" onClick={this.props.nextSlide}>
                <img className="team-logo" src={this.props.homeLogo}/>

                <div className="team-name">{this.props.homeName}</div>
                <div className="score-widget">{this.props.children}</div>
                <div className="team-name is-away">{this.props.awayName}</div>
                <img className="team-logo" src={this.props.awayLogo}/>
            </div>
        );
    }
});

/**
 * Event item shown in MatchListModule when a event occurs
 *
 * @constructor
 * @param {function} nextSlide - debug callback only for test purpose
 * @param {string} eventType - type of event that occurred
 * @param {number=} timestamp - minutes after match start, the event occurred.
 * @param {string} text - description of the event
 * @param {string=} subText - additional description of the event
 */
var MatchListItemEvent = React.createClass({
    propTypes: {
        eventType: React.PropTypes.oneOf(["substitution", "goal", "yellow-card", "red-card", "yellow-red-card", "penalty"]),
        subText: React.PropTypes.string
    },
    render: function () {
        var timeBox = this.props.timestamp ? (
                <div className="time-box-pattern is-light">{this.props.timestamp}'</div>) : [],
            subText = this.props.subText ? (<small>{this.props.subText}</small>) : [];

        return (
            <div className="match-list-event-item" onClick={this.props.nextSlide}>
                <div className={"event-icon-pattern is-" + this.props.eventType}></div>
                <div className="event-text">{this.props.text} {subText}</div>
                {timeBox}
            </div>
        );
    }
});


/**
 * A list item wrapper for the content of MatchListModule.
 *
 * Shows item based on the currentSlide state.
 *
 * if currentSlide is:
 *   0 - shows a MatchListItemScoreboard
 *   1 - shows a MatchListItemEvent
 *   2 - shows a MatchListItemBanner
 *
 * @constructor
 * @param {string} homeName: name of the home team
 * @param {string} awayName: name of the visiting team
 * @param children - one Scoreboard
 */
var MatchListItem = React.createClass({
    propTypes: {
        homeName: React.PropTypes.string.isRequired,
        awayName: React.PropTypes.string.isRequired
    },
    getInitialState: function () {
        return {
            currentSlide: 0
        }
    },
    nextSlide: function (e) {
        var nextSlide = this.state.currentSlide + 1;
        if (nextSlide > 2) nextSlide = 0;
        this.setState({currentSlide: nextSlide});
    },
    render: function () {
        var content = [];

        switch (this.state.currentSlide) {
            case 0 :
                content = (
                    <MatchListItemScoreboard homeName={this.props.homeName} awayName={this.props.awayName}
                                             nextSlide={this.nextSlide} key={1}>
                        {this.props.children}
                    </MatchListItemScoreboard>);
                break;
            case 1 :
                content = (
                    <MatchListItemEvent nextSlide={this.nextSlide} eventType="substitution" text="Name Namesen"
                                        subText="Name Namesen" timestamp={37} key={3}/>);
                break;
            case 2 :
                content = (<MatchListItemBanner nextSlide={this.nextSlide} key={2}/>);
                break;
        }

        return (
            <li className="match-list-item">
                <ReactCSSTransitionGroup transitionName="slide" component="div" className="cube-wrapper" transitionLeaveTimeout={500} transitionEnterTimeout={500}>
                    {content}
                </ReactCSSTransitionGroup>
            </li>
        );
    }

});


/**
 * List of matches.
 *
 * @constructor
 * @param {string} title - lists headline
 * @param children - zero or more MatchListItem
 */
var MatchListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function () {
        return (
            <section className="match-list-module">
                <h3><span className="col-title">{this.props.title}</span></h3>
                <ul>
                    {this.props.children}
                </ul>
            </section>
        );
    }
});