var FixedHeaderMixin = {

    componentWillMount: function () {
        this.containers = [];
    },

    componentWillUnmount: function () {
        if (this.container.length > 0) window.removeEventListener('scroll', this.handleScroll);
        this.containers = null;
    },

    addFixedHeader: function (container, header, topOffset) {
        this.containers.push({
            container: container,
            header: header,
            topOffset: topOffset
        });

        // be sure container has position relative
        container.style.position = "relative";

        // add scroll event listener if this is the first added fixed header
        if (this.containers.length === 1) {
            window.addEventListener('scroll', this.handleScroll);
        }
    },

    getOffsetRect: function (elem) {
        var box = elem.getBoundingClientRect(),
            body = document.body,
            docElem = document.documentElement,
            scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop,
            scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft,
            clientTop = docElem.clientTop || body.clientTop || 0,
            clientLeft = docElem.clientLeft || body.clientLeft || 0,
            top = box.top + scrollTop - clientTop,
            left = box.left + scrollLeft - clientLeft;

        return {top: Math.round(top), left: Math.round(left)}
    },

    handleScroll: function () {
        if (this.lock) return;
        this.lock = true;

        this.containers.map( fh => {
            var container = fh.container,
                header = fh.header,
                sTop = document.body.scrollTop + fh.topOffset,
                cTop = this.getOffsetRect(container).top,
                cBottom = cTop + container.clientHeight;

            if (sTop <= cTop) {
                header.style.cssText = "position: relative";
                container.style.paddingTop = "";
            } else if (sTop > cBottom - header.clientHeight) {
                header.style.cssText = "position: absolute; bottom: 0; width: 375px";
                container.style.paddingTop = "";
            } else {
                header.style.cssText = "position: fixed; top: " + fh.topOffset + "px ; left: 0; width: 375px";
                container.style.paddingTop = header.clientHeight + "px";
            }
        });

        this.lock = false;
    }

};
