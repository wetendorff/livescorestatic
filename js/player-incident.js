var IncidentEnum = {
    "None": 0,
    "Goal": 1,
    "Yellow": 2,
    "Red": 4,
    "Yellow2red": 8,
    "Substitute": 16
};

var PlayerIncidents = React.createClass({
    propTypes: {
      incidents: React.PropTypes.number
    },

    render: function () {
        var incidentArr = [],
            incidents = this.props.incidents || IncidentEnum.None;

        if( incidents & IncidentEnum.Goal ) {
            incidentArr.push(<i className={"icon-ball icon-ball-sm"} key={1}></i>);
        }
        if( incidents & IncidentEnum.Yellow ) {
            incidentArr.push(<i className={"icon-yellow-card icon-yellow-card-sm"} key={2}></i>);
        }
        if( incidents & IncidentEnum.Red ) {
            incidentArr.push(<i className={"icon-red-card icon-red-card-sm"} key={3}></i>);
        }
        if( incidents & IncidentEnum.Yellow2red) {
            incidentArr.push(<i className={"icon-red-card-2-yellow icon-red-card-2-yellow-sm"} key={4}></i>);
        }
        if( incidents & IncidentEnum.Substitute) {
            incidentArr.push(<i className={"icon-substitute icon-substitute-sm"} key={5}></i>);
        }

        return (
            <div className="player-incident-pattern">{incidentArr}</div>
        );
    }
});