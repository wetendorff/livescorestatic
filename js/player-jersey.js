var PlayerJersey = React.createClass({
    propTypes: {
        jerseyUrl: React.PropTypes.string.isRequired,
        number: React.PropTypes.number,
        location: React.PropTypes.oneOf(['home', 'away']).isRequired
    },

    render: function () {
        var jerseyClsName = "player-jersey-pattern is-" + this.props.location;

        return (
            <div className={jerseyClsName}>
                <img src={this.props.jerseyUrl} alt="" width="50" height="50"/>
                <span className="player-number">{this.props.number}</span>
            </div>
        );
    }
});