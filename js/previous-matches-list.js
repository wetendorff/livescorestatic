/**
 * Match bubble. Bubble showing status of a match.
 *
 * @constructor
 * @param {string} state - if the match was lost, tie or won
 * @param {string} location - if the match was a home game or away game
 * @param {boolean=} isLastMatch - if the match is the last played
 */
var PreviousMatchBubble = React.createClass({
    propTypes: {
        state: React.PropTypes.oneOf(['lost','tie','won']).isRequired,
        location: React.PropTypes.oneOf(['home', 'away']).isRequired,
        isLastMatch: React.PropTypes.bool
    },

    getInitialState: function () {
        return {
            stateClasses: {'lost': 'is-lost', 'tie': 'is-tie', 'won': 'is-won'},
            stateText: {'lost': 'T', 'tie': 'U', 'won': 'V'}
        };
    },

    render: function () {
        var clsName = "match-bubble-pattern " + this.state.stateClasses[this.props.state];

        if (this.props.location === 'home') clsName += " is-home";
        if (this.props.isLastMatch === true) clsName += " is-lg";

        return (
            <li className={clsName}>{this.state.stateText[this.props.state]}</li>
        )
    }
});


/**
 * Generate a list item block of match history bubbles for a single team.
 *
 * @constructor
 * @param {string} team - name of the team
 * @child
 */
var PreviousMatchesListItem = React.createClass({
    propTypes: {
        team: React.PropTypes.string
    },

    render: function () {
        var children = React.Children.count(this.props.children) > 0 ?
            this.props.children : (
                <li>Ingen data</li>
            );

        return (
            <div>
                <h4 className="team-name">{this.props.team}</h4>
                <ul className="matches">
                    {children}
                </ul>
            </div>
        );
    }
});


/**
 * PreviousMatchesListModule - list with previous matches history.
 *
 * @constructor
 * @param {string} title - the lists headline
 * @param children - zero or more PreviousMatchesListItem
 */
var PreviousMatchesListModule = React.createClass({
    propTypes: {
        title:  React.PropTypes.string.isRequired,
        children: React.PropTypes.node
    },

    render: function () {
        var children = React.Children.count(this.props.children) > 0 ?
            this.props.children : (
                <div className="no-data">Ingen data</div>
             );

        return (
            <section className="previous-matches-module">
                <h3><span className="col-title">{this.props.title}</span></h3>
                {children}
            </section>
        );
    }
});
