/**
 * Visual presentation of match and pause time.
 *
 * The break timer is only show if first or second half, and minutes is 45.
 *
 * @constructor
 * @param {number} minutes - number of minutes passed since the current half started. Must be a value between 0 - 45
 * @param {number} breakMinutes - number of minutes passed since the current half ended. Value between 0 - 15
 * @param {number} half - the current half. 0 = first half, 1 = second half, 2 = extra time
 */
var ScoreProgress = React.createClass({
    propTypes: {
        minutes: function (props, propName, componentName) {
            var v = props[propName];
            if (typeof v !== 'number' || ( v < 0 || v > 45)) {
                return new Error(propName + ' is not of type number or is not in range 0-120!');
            }
        },
        breakMinutes: function (props, propName, componentName) {
            var v = props[propName];
            if (typeof v !== 'number' || ( v < 0 || v > 15)) {
                return new Error(propName + ' is not of type number or is not in range 0-15!');
            }
        },
        half: React.PropTypes.oneOf([0, 1, 2]).isRequired
    },

    render: function () {
        var w = 200,
            h = 200,
            viewBox = [0, 0, w, h].join(" "),
            c1, c2,
            halfTimeInMinutes = (this.props.half === 2 ? 30 : 45),
            halfTimeInDegrees = (this.props.half === 2 ? 180 : 270),
            breakInMinutes = 15,
            timeDeg,
            breakDeg = 0;

        // be sure deg is between 0 and halfTimeInDegree
        timeDeg = Math.min(Math.max(halfTimeInDegrees * this.props.minutes / halfTimeInMinutes, 0), halfTimeInDegrees);

        // create haft time circle
        c1 = this.createCircle(100, 100, 95, 0, timeDeg, "#CBFE33");

        // create break time circle only if first or second half
        if (this.props.half === 0 || this.props.half === 1) {
            breakDeg = Math.max(Math.min(90 * this.props.breakMinutes / breakInMinutes, 90), 0);
            c2 = this.createCircle(100, 100, 95, 270, halfTimeInDegrees + breakDeg, "#639F42");
        }

        // create time mark
        var mark = (this.props.half === 2 ) ?
            (<line x1={w-10} y1={h/2} x2={w} y2={h/2} stroke="black" strokeWidth="1"/>) :
            (<line x1="0" y1={h/2} x2="10" y2={h/2} stroke="black" strokeWidth="1"/>);

        // calculate spark position and color
        var sparkPos = this.polarToCartesian(100, 100, 95, timeDeg + breakDeg);
        var sparkID = ( timeDeg + breakDeg > 270 ) ? "url(#BreakSparkGradient)" : "url(#SparkGradient)";

        return (
            <svg viewBox={viewBox} version="1.1" className="score-progress">
                <defs>
                    <radialGradient id="SparkGradient">
                        <stop offset="0%" stopColor="#CBFE33" stopOpacity="1"/>
                        <stop offset="100%" stopColor="#CBFE33" stopOpacity="0"/>
                    </radialGradient>
                    <radialGradient id="BreakSparkGradient">
                        <stop offset="0%" stopColor="#639F42" stopOpacity="1"/>
                        <stop offset="100%" stopColor="#639F42" stopOpacity="0"/>
                    </radialGradient>
                </defs>
                <circle cx="100" cy="100" r="95" fill="#000" stroke="#A9A9A9" strokeWidth="10"/>
                {c1}
                {c2}
                {mark}
                <circle cx={sparkPos.x} cy={sparkPos.y} r="15" fill={sparkID}/>
            </svg>
        );
    },
    createCircle: function (x, y, radius, startAngle, endAngle, color) {
        if (startAngle === 0 && endAngle == 360) {
            return (
                <circle cx={x} cy={y} r={radius} fill="none" stroke={color} strokeWidth="10"/>
            )
        }

        var start = this.polarToCartesian(x, y, radius, endAngle),
            end = this.polarToCartesian(x, y, radius, startAngle),
            arcSweep = endAngle - startAngle <= 180 ? "0" : "1",
            path = ["M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0, end.x, end.y].join(" ");

        return (
            <path d={path} fill="none" stroke={color} strokeWidth="10"/>
        )
    },
    polarToCartesian: function (centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
});


/**
 * Visual presentation of a match score before, during and after a match.
 *
 * Only the first 8 red cards are shown for a given team.
 *
 * @constructor
 * @param {status=} status - status of the match. Can be "before", "after" and "during". "before" is default
 * @param {number=} half - the current half. 0(default) = first half, 1 = second half, 2 = extra time
 * @param {number=} time - number of minutes since the beginning of the match without stoppage time and breaks. Default is 0.
 * @param {number=} homeScore - number of goals the home team has scored. Default is 0
 * @param {number=} awayScore - number of goals the away team has scored. Default is 0
 * @param {bool=} isBreak - current half has ended and waiting on the next half. default is false
 * @param {number=} breakTime - number of minutes passed since the current half ended. Can be a value between 0 - 15. Default is 0
 * @param {bool=} isSmall - render small version of the scoreboard. Default is true
 * @param {number=} homeRedCards - red cards the home team has received. Default is 0
 * @param {number=} awayRedCards - red cards the away team has received. Default is 0
 */
var Scoreboard = React.createClass({
    propTypes: {
        status: React.PropTypes.oneOf(["before", "after", "during"]),
        time: React.PropTypes.number,
        homeScore: React.PropTypes.number,
        awayScore: React.PropTypes.number,
        half: React.PropTypes.number,
        isBreak: React.PropTypes.bool,
        isSmall: React.PropTypes.bool,
        breakTime: React.PropTypes.number,
        homeRedCards: React.PropTypes.number,
        awayRedCards: React.PropTypes.number
    },

    getDefaultProps: function () {
        return {
            status: "before",
            time: 0,
            homeScore: 0,
            homeRedCards: 0,
            awayScore: 0,
            awayRedCards: 0,
            half: 0,
            breakTime: 0,
            isBreak: false,
            isSmall: false
        }
    },

    render: function () {
        var scoreClsName = "score-module score-module-" + this.props.status + (this.props.isSmall ? " score-module-small" : ""),
            homeScoreClsName = "home-score",
            awayScoreClsName = "away-score",
            time = this.props.time,
            halfTimeInMinutes = 45,
            extraTimeInMinutes = 30,
            scoreProgress = [],
            minutes = 0,
            topText = "",
            centerText = "",
            bottomText = "",
            breakTime,
            redCardBarHome, redCardBarAway,
            redCardHomeNo = Math.max(Math.min(this.props.homeRedCards, 8),0), // min 0 max 8
            redCardAwayNo = Math.max(Math.min(this.props.awayRedCards, 8),0); // min 0 max 8

        if (this.props.half === 0) {
            minutes = Math.min(time, halfTimeInMinutes);
        } else if (this.props.half === 1) {
            minutes = Math.min(time - halfTimeInMinutes, halfTimeInMinutes);
        } else if (this.props.half === 2) {
            minutes = Math.min(time - halfTimeInMinutes * 2, extraTimeInMinutes);
        }

        breakTime = this.props.isBreak && halfTimeInMinutes === minutes ? this.props.breakTime : 0;

        if( redCardHomeNo > 0 ) {
            redCardBarHome = (<div className={"card-bar cards-" + redCardHomeNo}></div>);
        }
        if( redCardAwayNo > 0 ) {
            redCardBarAway = (<div className={"card-bar is-flipped cards-" + redCardAwayNo}></div>);
        }

        switch (this.props.status) {
            case "before":
                topText = "Kickoff";
                centerText = "10";
                bottomText = "Min.";
                break;
            case "during":
                topText = "LIVE";
                centerText = this.props.isBreak ? "PAUSE" : this.props.time + "´";
                bottomText = ["1.H", "2.H", "FORLÆNGET"][this.props.half];
                scoreProgress = (
                    <ScoreProgress half={this.props.half} minutes={minutes} breakMinutes={breakTime}/>
                );
                break;
            case "after":
                topText = "FT";
                centerText = "90+3";
                bottomText = "Min.";

                if (this.props.homeScore > this.props.awayScore) {
                    homeScoreClsName += " is-won";
                    awayScoreClsName += " is-lost";
                } else if (this.props.homeScore == this.props.awayScore) {
                    homeScoreClsName += " is-tie";
                    awayScoreClsName += " is-tie"
                } else if (this.props.homeScore < this.props.awayScore) {
                    homeScoreClsName += " is-lost";
                    awayScoreClsName += " is-won";
                }

                break;
        }

        return (
            <div className={scoreClsName}>
                <div className="board">
                    <div className={homeScoreClsName}>
                        {redCardBarHome}
                        {this.props.status === 'before' ? "-" : this.props.homeScore}
                    </div>
                    <div className={awayScoreClsName}>
                        {redCardBarAway}
                        {this.props.status === 'before' ? "-" : this.props.awayScore}
                    </div>
                </div>
                <div className="progress">
                    <span className="txt-top">{topText}</span>
                    <span className="txt-center">{centerText}</span>
                    <span className="txt-bottom">{bottomText}</span>
                    {scoreProgress}
                </div>
            </div>
        );
    }

});
