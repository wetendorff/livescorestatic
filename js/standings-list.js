var StandingsListModule = React.createClass({
   render: function() {
        return (
            <section className="standings-list-module" ref="container">
                <h3 ref="header">
                    <span className="col-title">Stilling</span>
                    <span className="col-matches is-mute">K</span>
                    <span className="col-won is-mute">V</span>
                    <span className="col-tie is-mute">U</span>
                    <span className="col-lost is-mute">T</span>
                    <span className="col-balance is-mute">+/-</span>
                    <span className="col-points">Point</span>
                </h3>
                <ul>
                    <li>
                        <div className="col-position">
                            <span className="pos-badge-pattern is-up-0">1</span>
                        </div>
                        <div className="col-history is-up"></div>
                        <div className="col-team">
                            Borussia Mönchengladbach
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+13</div>
                        <div className="col-points">33</div>
                    </li>
                    <li className="is-active">
                        <div className="col-position"><span className="pos-badge-pattern is-up-1">2</span>
                        </div>
                        <div className="col-history is-down"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern is-up-2">3</span>
                        </div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern is-up-3">4</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern">5</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li className="is-active">
                        <div className="col-position"><span className="pos-badge-pattern">6</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern">7</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern is-down-0">8</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern is-down-1">9</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                    <li>
                        <div className="col-position"><span className="pos-badge-pattern is-down-1">10</span></div>
                        <div className="col-history"></div>
                        <div className="col-team">
                            IFK Norrköping
                        </div>
                        <div className="col-matches">14</div>
                        <div className="col-won">7</div>
                        <div className="col-tie">3</div>
                        <div className="col-lost">4</div>
                        <div className="col-balance">+14</div>
                        <div className="col-points">30</div>
                    </li>
                </ul>
            </section>
        );
   }
});