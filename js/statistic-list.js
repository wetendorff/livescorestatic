var StatisticListItem = React.createClass({
    propTypes: {
        caption: React.PropTypes.string.isRequired,
        valueLabel: React.PropTypes.string.isRequired,
        percent: React.PropTypes.number.isRequired      // value between 0 and 100
    },

    render: function () {
        return (
            <li>
                <div className="headline">
                    <span className="caption">{this.props.caption}</span>
                    <span className="number">{this.props.valueLabel}</span>
                </div>
                <div className="progress-bar-pattern">
                    <span style={{width: this.props.percent + "%"}}></span>
                </div>
            </li>
        );
    }

});

var StatisticListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function () {
        return (
            <section className="statistic-list-module">
                <h3><span className="col-title">{this.props.title}</span></h3>
                <ul>
                    {this.props.children}
                </ul>
            </section>
        );
    }
});