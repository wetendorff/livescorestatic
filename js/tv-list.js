var TVListItem = React.createClass({


   render: function() {
       return (
           <li>
               <img className="col-logo" src={this.props.logo} alt="" />

               <div className="col-program">
                   {this.props.home}<br/>
                   {this.props.away}
                   <small>{this.props.league}</small>
               </div>
               <div className="col-time">
                   {this.props.start}
                   <small>({this.props.matchStart})</small>
               </div>
           </li>
       );
   }
});


var TVListModule = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired
    },

    render: function() {
        return (
            <section className="tv-list-module">
                <h3>
                    <span className="col-title">I dag, mandag</span>
                    <span className="col-date">11. november</span>
                </h3>
                <ul>
                    <TVListItem logo="http://srt.jppol.dk/tvguide/svglogo/eurosport-4F.svg"
                        home="Lech Poznan"
                        away="LKS Lodz"
                        league="1. Division, Polen"
                        start="18:30"
                        matchStart="19:45" />

                    <TVListItem logo="http://srt.jppol.dk/tvguide/svglogo/Sport_1-4F.svg"
                        home="Dulsburg"
                        away="Fortuna Dusseldorf"
                        league="2. Bundesliga, Tyskland"
                        start="18:30"
                        matchStart="20:00" />
                </ul>
            </section>
        );
    }
});
